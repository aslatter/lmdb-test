import * as lmdb from "node-lmdb";

import * as fs from "fs";

export function main() {
    // Make demo directory
    fs.mkdirSync("./db");

    withEnv({ path: "./db", maxDbs: 3 }, env => {
        const dbi = env.openDbi({ keyIsBuffer: true });
    });
}

function withEnv(options: lmdb.EnvOpenOptions, fn: (env: lmdb.Env) => void) {
    let env: lmdb.Env | undefined;
    try {
        env = new lmdb.Env();
        env.open(options);
        fn(env);
    } finally {
        if (env) {
            env.close();
        }
    }
}

function set(txn: lmdb.Txn, dbi: lmdb.Dbi, key: string[], value: string): void {
    txn.putString(dbi, encodeKey(key), value, { keyIsBuffer: true });
}

function get(txn: lmdb.Txn, dbi: lmdb.Dbi, key: string[]): string | null {
    return txn.getString(dbi, encodeKey(key), { keyIsBuffer: true });
}

function kill(txn: lmdb.Txn, dbi: lmdb.Dbi, key: string[]): void {
    txn.del(dbi, encodeKey(key), { keyIsBuffer: true });
}

function killTree(txn: lmdb.Txn, dbi: lmdb.Dbi, key: string[]): void {
    // TODO
}

function encodeKey(keyParts: string[]): Buffer {
    return Buffer.concat([
        ...keyParts.map(key => formatString(key)),
        Buffer.alloc(1, 0),
    ]);
}

export function encodeKeyPrefix(keyParts: string[]): Buffer {
    return Buffer.concat([...keyParts.map(key => formatString(key))]);
}

function formatString(key: string): Buffer {
    let toEncode = key;
    if (toEncode.match(/\x00|\x01/g)) {
        toEncode = toEncode
            .replace(/\x01/g, "\x01\x01")
            .replace(/\x00/g, "\x01");
    }
    return Buffer.from("m" + toEncode + "\x00", "utf8");
}
