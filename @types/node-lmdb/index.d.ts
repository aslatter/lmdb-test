declare module "node-lmdb" {
    export type EnvOpenOptions = {
        path: string;
        maxDbs: number;

        mapSize?: number;

        // flags

        noSubdir?: boolean;
        readOnly?: boolean;
        useWritemap?: boolean;
        noMetaSync?: boolean;
        noSync?: boolean;
        mapAsync?: boolean;
        unsafeNoLock?: boolean;
    };

    export type EnvOpenDbiOptions = {
        name?: string;
        reverseKey?: boolean;
        dupSort?: boolean;
        dupFixed?: boolean;
        integerDup?: boolean;
        reverseDup?: boolean;
        create?: boolean;
    } & KeyOptions;

    export class Env {
        public open(options: EnvOpenOptions): void;

        public close(): void;

        public resize(mapSize: number): void;

        public stat(): {
            pageSize: number;
            treeDepth: number;
            treeBranchPageCount: number;
            treeLeafPageCount: number;
            entryCount: number;
        };

        public info(): {
            mapAdderss: number;
            mapSize: number;
            lastPageNumber: number;
            lastTxnId: number;
            maxReaders: number;
            numReaders: number;
        };

        public beginTxn(options?: { readOnly?: boolean }): Txn;

        public openDbi(options?: EnvOpenDbiOptions): Dbi;

        public sync(callback: (error: Error | null) => void): void;
    }

    type KeyOptions = {
        keyIsUint32?: boolean;
        keyIsBuffer?: boolean;
        keyIsString?: boolean;
    };

    export class Txn {
        public commit(): void;
        public abort(): void;
        public reset(): void;
        public renew(): void;

        // getString
        public getString(
            dbi: Dbi,
            key: KeyType,
            options?: KeyOptions,
        ): string | null;
        // getStringUnsafe
        // getBinary
        // getBinaryUnsafe
        // getNumber
        // getBoolean
        // getString
        // putString
        public putString(
            dbi: Dbi,
            key: KeyType,
            value: string,
            options?: KeyOptions,
        ): void;
        // putBinary
        // putNumber
        // putBoolean
        // del
        public del(dbi: Dbi, key: KeyType, options?: KeyOptions): void;
    }

    export class Dbi {
        public close(): void;

        public drop(options?: { justFreePages?: boolean; txn?: Txn }): void;

        public stat(
            txn: Txn,
        ): {
            pageSize: number;
            treeDepth: number;
            treeBranchPageCount: number;
            treeLeafPageCount: number;
            entryCount: number;
        };
    }

    export const version: {
        versionString: string;
        major: number;
        minor: number;
        patch: number;
    };

    type KeyType = string | number | boolean | Buffer;
}
